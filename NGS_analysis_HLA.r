#Visulalization of HLA amplicon coverage 

#Library 
library(Rsamtools)
library(GenomicRanges)
library(GenomicAlignments)
library(ggbio)
library(Homo.sapiens)



#Loading the bam files (test with 1 or 2 first)
run1_12 <- BamFile("./hla_analysis/data/HLA_run1_BC12.sorted.bam", index = "./hla_analysis/data/HLA_run1_BC12.sorted.bai")
seqinfo(run1_12)
countBam(run1_12)
run1_24 <- BamFile("./hla_analysis/data/HLA_run1_BC24.sorted.bam")
seqinfo(run1_24)
countBam(run1_24)

x <- readGAlignments(run1_12, use.names=TRUE, 
                     param=ScanBamParam(which=GRanges("chr6", IRanges(29910309, 33041378))))
y <- readGAlignments(run1_24, use.names=TRUE, 
                     param=ScanBamParam(which=GRanges("chr6", IRanges(29910309, 33041378))))
xcov <- coverage(x)
ycov <- coverage(y) 
#Zoom in the region of interest (HLA-A, chr6:29942532,29945870)
hla_A <- GRanges("chr6", IRanges(29942532-1000,29945870+1000))

#first plot of the coverage 1
xnum <- as.numeric(xcov$chr6[ranges(hla_A)])
ynum <- as.numeric(ycov$chr6[ranges(hla_A)])
ggplot(x)+geom_point()


plot(xnum, type="l", col="blue", lwd=2)
lines(ynum, col="red", lwd=2)
legend(x = "bottomright",          # Position
       legend = c("run1 BC12", "run1 BC24"),
       col = c("blue", "red"),# Legend texts
       lwd = 2) 

#Other vizualisation, might be usefule at some point don't know yet 
data(genesymbol, package = "biovizBase")
whgene <- genesymbol[c("HLA-A", "HLA-B", "HLA-C")]
whgene2<- genesymbol[c("HLA-DPA1", "HLA-DPB1", "HLA-DQA1", "HLA-DQB1", "HLA-DRB1","HLA-DRB3", "HLA-DRB4", "HLA-DRB5")]
wh <- range(whgene, ignore.strand = TRUE)
wh2 <- range(whgene2, ignore.strand = TRUE)

# autoplot(Homo.sapiens, which = wh)
wh <- keepSeqlevels(wh, "chr6", pruning.mode="coarse")
wh2 <- keepSeqlevels(wh2, "chr6", pruning.mode="coarse")

# autoplot(run1_12, which=wh)

ga <- readGAlignments(run1_12, use.names=TRUE, 
                      param=ScanBamParam(which=GRanges("chr6", IRanges(31541378, 33041378))))

p1 <- autoplot(ga, geom = "rect")
p2 <- autoplot(ga, geom = "line", stat = "coverage")
# vcf <- readVcf(file="data/varianttools_gnsap.vcf", genome="ATH1")
# p3 <- autoplot(vcf[seqnames(vcf)=="Chr5"], type = "fixed") + xlim(4000, 8000) + theme(legend.position = "none", axis.text.y = element_blank(), axis.ticks.y=element_blank())
# txdb <- makeTxDbFromGFF(file="./data/TAIR10_GFF3_trunc.gff", format="gff3")
p4 <- autoplot(Homo.sapiens, which = wh2, names.expr =  "SYMBOL", mode='reduce')
tracks(Reads=p1, Coverage=p2, Transcripts=p4, heights = c(0.4, 0.2, 0.5)) + ylab("")

wh <- keepSeqlevels(wh, "chr6", pruning.mode="coarse")

ga2 <- readGAlignments(run1_24, use.names=TRUE, 
                       param=ScanBamParam(which=GRanges("chr6", IRanges(29910309, 33041378))))

autoplot(ga, geom = "line", stat = "coverage", fill="brown")

colors<-c("BC12"="red", "BC24"="green")
ggplot()+stat_coverage(run1_12, which=wh, aes(run1_12 ,color="red"))+stat_coverage(run1_24, which=wh, color="darkgreen")+
  labs(color = "Legend") +
  scale_color_manual(values = colors)
