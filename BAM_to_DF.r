#BAMFile to data frame 
#load library
library(Rsamtools)

#read in entire BAM file
bam <- scanBam("./hla_analysis/data/HLA_run1_BC12.sorted.bam")
#store names of BAM fields
bam_field <- names(bam[[1]])
# [1] "qname"  "flag"   "rname"  "strand" "pos"    "qwidth" "mapq"   "cigar"  "mrnm"   "mpos"   "isize"  "seq"   
# [13] "qual" 


#distribution of BAM flags
table(bam[[1]]$flag)
#0    4   16  256  272 2048 2064 
#4951   27 4899  749  809  493  503 

#function for collapsing the list of lists into a single list
#as per the Rsamtools vignette
.unlist <- function (x){
  ## do.call(c, .) coerces factor to integer, which is undesired
  x1 <- x[[1L]]
  if (is.factor(x1)){
    structure(unlist(x), class = "factor", levels = levels(x1))
  } else {
    do.call(c, x)
  }
}

#go through each BAM field and unlist
list <- lapply(bam_field, function(y) .unlist(lapply(bam, "[[", y)))

#store as data frame
bam_df <- do.call("data.frame", list)
names(bam_df) <- bam_field

dim(bam_df)

#use chr6 as an example
#how many entries on the negative strand of chr22?
table(bam_df$rname == 'chr6' & bam_df$flag == 16)
# FALSE  TRUE 
# 8731  3700 

test <- subset(bam_df, rname == 'chr6')
dim(test)

ggplot(test) + geom_density(aes(x=pos))
