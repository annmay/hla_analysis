#!/bin/bash
#SBATCH --time=00:45:00
#SBATCH --mem=16G
#SBATCH --account=def-ioannisr



module load nixpkgs/16.09
# module load python/3.6.3
module load gatk/4.1.2.0

cd ~/scratch/HLA/references/

# gatk FastaReferenceMaker -R $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa -O chr6.fasta -L chr6
gatk FastaReferenceMaker -R $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa -O chr6_HLA.fasta -L ./HLA_gene.bed
