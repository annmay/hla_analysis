#!/bin/bash
#SBATCH --time=00:15:00
#SBATCH --mem=1G
#SBATCH --account=def-ioannisr

module load nixpkgs/16.09
module load gatk/4.1.2.0

cd ~/scratch/HLA/alignment/

cd
for dir in */; do
  cd $dir
  FILE=${dir::-1}
  echo $FILE,"GATK Started"
  gatk CountReads -I $FILE.sorted.bam -L ~/scratch/HLA/HLA_gene.bed > ~/scratch/HLA/metrics/$FILE.sorted.bam.CountReadsHLA
  cd ../
done
