"""
HLA_Amplicon
    Code to generate metrics of the amplicons from the
    bed file containing the region coordinate
Anne-Marie Roy
Summer 2021
"""

import re, os, csv, statistics, sys
import pybedtools as pbt
import pandas as pd
from os import listdir, path
from pathlib import Path

###VARIABLE FOR THE BED FILE
BedPath = "./HLA_gene.bed"

"""
The script needs 2 arguments in entry
1: Input directory -> /home/aroy4/scratch/HLA/AmpTemp/ (default)
2: Output directory -> /home/aroy4/scratch/HLA/AmpMetrics/ (default)
"""

#Amplicons Expected Lenght
HLA_A = 3000      #Lenght:  3338
HLA_B = 3000      #Lenght:  3304
HLA_C = 3000      #Lenght:  3343
HLA_DPA1 = 5500   #Lenght:  16179
HLA_DPB1 = 6600   #Lenght:  13706
HLA_DQA1 = 5500   #Lenght:  17658
HLA_DQB1 = 6500   #Lenght:  7190
HLA_DRB1 = 4300   #Lenght:  100154
HLA_DRB3 = 4300   #Lenght:  13174
HLA_DRB5 = 5000   #Lenght:  45163

#find the min and max of each primer pair
def Average(lst):
    if len(lst) != 0:
        return sum(lst) / len(lst)
    else:
        return 0

def find_files(dir, motif):
    files = {}
    for f in listdir(dir):
        if motif in f: #select only the bam files of the directory
            files[str(f).split(".")[0]] = str(dir)+str(f)
            # print(files)
    return files

def define_region(file):
    region = {} #dict of amplicon region--> Name:[min(start), max(end)]
    # with open(file, "r") as f:
    f= pbt.BedTool(file)
    for line in f:
        print("Amplicon name: ", line[3], "\t\t\t Lenght: ", int(line[2])-int(line[1]))
        region[line[3]]=[int(line[1]),int(line[2])]
    # print(region)
    return region


def meanDepth(regions, DepthFile): #DepthFile is the bam file with the calculated depth of each positions
    depths = {}
    averages = {}
    somme2 = 0
    for key, value in regions.items():
        depths[key] = []
        with open(DepthFile, "r") as f:
            for line in f:
                if (int(line.split()[1]) >= value[0]) and (int(line.split()[1]) <= value[1]):
                    depths[key] += [int(line.split()[2])]
    for key2, value2 in depths.items():
        averages[key2] = Average(value2)
        somme2 += sum(value2)
    return averages, depths

def Abundance(DepthPerRegion):
    abndt1 = {}
    NrmAvg = {}
    TotalBase = 0 #total amount of base in the sample
    for key, value in DepthPerRegion.items():
        TotalBase = TotalBase + sum(value)
    if TotalBase != 0:
        for key, value in DepthPerRegion.items():
            abndt1[key] = (sum(value)/TotalBase)
            NrmAvg[key] = (Average(value)/TotalBase)
    else:
        print(name)
    return abndt1, NrmAvg, TotalBase

def get_pctN(file_dict):
    pctN = {}
    for value in file_dict.values(): #key = name; value = path
        with open(value, "r") as f:
             reader = csv.reader(f, delimiter = ",")
             list_rows = list(reader)
             for row in list_rows:
                 if row[0] != "sample":
                     try:
                         pctN[row[0]] = round(float(row[1]),4)
                     except ValueError:
                         pctN[row[0]] = 0
    return pctN

def MinCov(DepthPerRegion, Number_pb): #DepthPerRegion is a dicto of regionName: position depth (in order)
    mincov = {}
    for key, value in DepthPerRegion.items():
        minimum = Average(value)
        for i in range(len(value)-Number_pb):
            if Average(value[i:i+Number_pb]) <= minimum:
                minimum = Average(value[i:i+Number_pb])
        mincov[key] = minimum
    return mincov

def FeatureCount(DepthPerRegion):
    FC = {}
    avg = {}
    try:
        maxV = list(DepthPerRegion.values())[0][0]
        minV = list(DepthPerRegion.values())[0][0]
    except IndexError:
        print("Failed, depth empty")
        maxV = 0
        minV = 100
    for key, value in DepthPerRegion.items():
        if value != []:
            if maxV <= max(value):
                maxV = max(value)
            if minV >= min(value):
                minV = min(value)
    for key, value in DepthPerRegion.items():
        listV = []
        if maxV-minV != 0:
            for v in value:
                v2 = (v-minV)/(maxV-minV)
                listV += [v2]
            FC[key] = listV
            avg[key] = Average(listV)
        else:
            print(key, minV, maxV)
            FC[key] = None
    return avg, FC

def writeDictToCSV(dict, path):
    df = pd.DataFrame(dict).T
    df.to_csv(path)


##Execution
inputdir = sys.argv[1] if len(sys.argv) >= 2 else '/home/aroy4/scratch/HLA/AmpTemp/'
outputdir = sys.argv[2] if len(sys.argv) >= 3 else '/home/aroy4/scratch/HLA/AmpMetrics/'
Path(outputdir).mkdir(parents=True, exist_ok=True)
region = define_region(BedPath)
print("region set")

# metric = find_files(inputdir, ".metrics.csv")
# print("metric files loaded")
# pctN = get_pctN(metric)
# print("%N dict made; Lenght: ", len(pctN.keys()))
files = find_files(inputdir, ".bam.depth")
print("Depth files loaded; Lenght: ", len(files.keys()))


statistics = {}
total_reads = {}
empty = []
rejected = {}

for name,value in files.items():
    if path.isfile(outputdir+"/Depth"+name+"statistics.csv"):
        print(name, " exist")
    else:
        print(name, " Started")
        avg, dpth = meanDepth(region, value)
        abndt_totalreads, NormAvg, Total = Abundance(dpth)
        print("Total bases: ", Total)
        avgFC, NormDpth = FeatureCount(dpth)
        min1 = MinCov(dpth, 1)
        min50 = MinCov(dpth, 50)
        min100 = MinCov(dpth, 100)
        NormMin50 = MinCov(NormDpth, 50)
        NormMin25 = MinCov(NormDpth, 25)
        NormMin1 = MinCov(NormDpth, 1)
        for key in region.keys():
            try:
                statistics[key] = {"avgDepth":avg[key], #Average coverage over all postion per amplicon
                # "abundance1":abndt_totalreads[key], # Sum of all position coverage per amplicon divided by total position coverage in sample
                # 'NormalizedAvgDepth':NormAvg[key], # Average coverage over all position per amplicon divided by total position coverage in sample
                "FeatureCount":avgFC[key], #Average coverage of feature count normalization for each position
                "MinCov_1pb":min1[key], #Minimun coverage observed over 1 position
                "MinCov_50pb":min50[key], #Minimun coverage observed over 50 consecutive positions
                "MinCov_100pb":min100[key], #Minimun coverage observed over 100 consecutive positions
                "NormMinCov_1pb":NormMin1[key], #Minimun coverage observed over 1 position
                "NormMinCov_25pb":NormMin25[key], #Minimun coverage observed over 50 consecutive positions
                "NormMinCov_50pb":NormMin50[key]} #Minimun coverage observed over 100 consecutive positions
            except KeyError:
                empty += [name, key]
                print(key, "is empty")
            writeDictToCSV(statistics, outputdir+"/Depth"+name+"statistics.csv")
            total_reads[name] = Total
        print(name, " _Completed, Total base reads: ", Total)

#Save Total reads per sample in a CSV
df = pd.DataFrame(total_reads, index = ["TotalCovPerPosition"]).T
if path.isfile(outputdir+"/Total_reads_stats.csv"):
    df1 = pd.read_csv(outputdir+"/Total_reads_stats.csv")
    df = df1.append(df)
df.to_csv(outputdir+"/Total_reads_stats.csv")
#Save rejected Samples to a CSV
writeDictToCSV(rejected, outputdir+"Rejected_Samples.csv")

#Save failedSample to a csv
with open(outputdir+'list_empty_sample.csv', 'w', newline='') as myfile:
     wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
     wr.writerow(empty)
