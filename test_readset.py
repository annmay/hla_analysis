import csv
import logging
import os
import re
# import ConfigParser

nanopore_readset_file = "./readset_nanopore2.txt"
readsets = []
samples = []

# log.info("Parse Nanopore readset file " + nanopore_readset_file + " ...")
readset_csv = csv.DictReader(open(nanopore_readset_file, 'r'), delimiter='\t')
for line in readset_csv:
    print("line", line)
    sample_name = line['Sample']
    print("Sample Name", sample_name)
    sample_names = [sample.name for sample in samples]
    if sample_name in sample_names:
        # Sample already exists
        sample = samples[sample_names.index(sample_name)]
    else:
        # Create new sample
        sample = Sample(sample_name)
        samples.append(sample)
