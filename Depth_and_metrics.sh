#!/bin/bash
#SBATCH --time=00:30:00
#SBATCH --mem=4G
#SBATCH --account=def-ioannisr


module load samtools/1.12
module load picard/2.23.3
module load bamtools/2.5.1
# module load gatk/4.1.8.1


cd ~/scratch/HLA
mkdir -p metrics


cd alignment/
for dir in */; do
  cd $dir
  FILE=${dir::-1}
  echo $FILE,"Started"
  samtools depth $FILE.sorted.bam > ~/scratch/HLA/metrics/$FILE.sorted.bam.depth | awk '$1 == "chr6" {print $0}'
  samtools bedcov ~/scratch/HLA/references/HLA_gene.bed $FILE.sorted.bam > ~/scratch/HLA/metrics/$FILE.sorted.bam.BedCov
  bamtools count $FILE.sorted.bam > ~/scratch/HLA/metrics/$FILE.sorted.bam.count
  bamtools stats $FILE.sorted.bam > ~/scratch/HLA/metrics/$FILE.sorted.bamStat
  java -jar $EBROOTPICARD/picard.jar CollectAlignmentSummaryMetrics -I $FILE.sorted.bam -O ~/scratch/HLA/metrics/$FILE.sorted.bam.picardMetrics
  cd ../
done


# cd ~/scratch/HLA/Targeted_analysis/alignment/
# for dir in */; do
#   cd $dir
#   FILE=${dir::-1}
#   echo $FILE,"Started"
#   samtools depth $FILE.sorted.bam > ~/scratch/HLA/metrics/$FILE.sorted.bam.depth
#   java -jar $EBROOTPICARD/picard.jar CollectAlignmentSummaryMetrics -I $FILE.sorted.bam -O ~/scratch/HLA/metrics/$FILE.sorted.bam.picardMetrics
#   cd ../
# done
